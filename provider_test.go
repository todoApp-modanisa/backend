package main

import (
	"backend/handler"
	mockRepository "backend/mocks/repository"
	"backend/models"
	"backend/repository"
	"backend/services"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/golang/mock/gomock"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

var mockRepo *mockRepository.MockTodoRepository
var repo repository.TodoRepository

func TestProvider_TodoGetPost(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "todo-list-backend",
	}

	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://localhost:8080",
		BrokerURL:                  "https://dilaraseker.pactflow.io/",
		BrokerToken:                "_pSn-LjTRkGZpZ0ttWvkIw",
		ProviderVersion:            "todo-app-provider-v1.0.0",
		PublishVerificationResults: true,

		BeforeEach: func() error {
			controller := gomock.NewController(t)
			defer controller.Finish()
			mockRepo = mockRepository.NewMockTodoRepository(controller)
			td := handler.TodoHandler{Service: services.NewTodoService(mockRepo)}

			app := fiber.New()
			app.Use(cors.New(cors.Config{
				AllowOrigins: "*",
				AllowMethods: "GET,POST,PATCH",
			}))
			app.Use(logger.New())
			app.Get("/todos", td.GetAllTasks)
			app.Post("/todos", td.CreateTask)
			go app.Listen(":3000")
			return nil

		},
		StateHandlers: types.StateHandlers{
			"3 tasks exists": func() error {
				_, err := repo.InsertRepository(models.Todo{ID: primitive.NewObjectID(), Task: "spor yap"})
				_, err = repo.InsertRepository(models.Todo{ID: primitive.NewObjectID(), Task: "markete git"})
				_, err = repo.InsertRepository(models.Todo{ID: primitive.NewObjectID(), Task: "okula git"})

				return err
			},
			"t1 task exists": func() error {
				_, err := repo.InsertRepository(models.Todo{ID: primitive.NewObjectID(), Task: "Spor yap"})

				return err
			},
		}},
	)
	t.Error(err)
}
