package main

import (
	"backend/configs"
	"backend/handler"
	"backend/repository"
	"backend/services"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {
	appRoute := fiber.New()
	appRoute.Use(cors.New(cors.Config{
		AllowCredentials: true,
		AllowOrigins:     "*",
		AllowHeaders:     "Origin, Content-Type, Accept, Accept-Language, Content-Length",
	}))
	configs.ConnectDB()
	dbClient := configs.GetCollection(configs.DB, "todos")

	TodoRepositoryDb := repository.NewTodoRepositoryDb(dbClient)

	teardown := handler.TodoHandler{Service: services.NewTodoService(TodoRepositoryDb)}

	appRoute.Post("/todos", teardown.CreateTask)
	appRoute.Get("/todos", teardown.GetAllTasks)
	appRoute.Listen(":8080")
}
