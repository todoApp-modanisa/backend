package services

import (
	"backend/mocks/repository"
	"backend/models"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"testing"
)

var mockRepo *repository.MockTodoRepository
var service TodoService

var mockData = []models.Todo{
	{primitive.NewObjectID(), "Task 1"},
	{primitive.NewObjectID(), "Task 2"},
	{primitive.NewObjectID(), "Task 3"},
}

func setup(t *testing.T) func() {
	controller := gomock.NewController(t)
	defer controller.Finish()

	mockRepo = repository.NewMockTodoRepository(controller)
	service = NewTodoService(mockRepo)
	return func() {
		service = nil
		defer controller.Finish()
	}
}
func TestDefaultTodoService_GetAllTasksService(t *testing.T) {
	td := setup(t)
	defer td()

	mockRepo.EXPECT().GetAllTasksRepository().Return(mockData, nil)
	result, err := service.GetAllTasksService()
	if err != nil {
		t.Error(err)
	}

	assert.NotEmpty(t, result)
}
func TestDefaultTodoService_InsertService(t *testing.T) {
	td := setup(t)
	defer td()

	todo := models.Todo{ID: primitive.NewObjectID(), Task: "Task 1"}

	mockRepo.EXPECT().InsertRepository(todo).Return(true, nil)
	result, err := service.InsertService(todo)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, result, true)
}
