package services

import (
	"backend/models"
	"backend/repository"
	_ "github.com/golang/mock/mockgen/model"
)

//go:generate mockgen -destination=../mocks/service/mockTodoservice.go -package=services backend/services TodoService
type DefaultTodoService struct {
	Repo repository.TodoRepository
}

type TodoService interface {
	InsertService(todo models.Todo) (bool, error)
	GetAllTasksService() ([]models.Todo, error)
}

func (t DefaultTodoService) InsertService(todo models.Todo) (bool, error) {
	var res bool
	res = true

	result, err := t.Repo.InsertRepository(todo)

	if err != nil || result == false {
		res = false
		return res, err
	}

	return res, nil
}

func (t DefaultTodoService) GetAllTasksService() ([]models.Todo, error) {
	result, err := t.Repo.GetAllTasksRepository()
	if err != nil {
		return nil, err
	}
	return result, nil
}

func NewTodoService(Repo repository.TodoRepository) DefaultTodoService {
	return DefaultTodoService{Repo: Repo}
}
