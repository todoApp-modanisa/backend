// Code generated by MockGen. DO NOT EDIT.
// Source: backend/services (interfaces: TodoService)

// Package services is a generated GoMock package.
package services

import (
	models "backend/models"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockTodoService is a mock of TodoService interface.
type MockTodoService struct {
	ctrl     *gomock.Controller
	recorder *MockTodoServiceMockRecorder
}

// MockTodoServiceMockRecorder is the mock recorder for MockTodoService.
type MockTodoServiceMockRecorder struct {
	mock *MockTodoService
}

// NewMockTodoService creates a new mock instance.
func NewMockTodoService(ctrl *gomock.Controller) *MockTodoService {
	mock := &MockTodoService{ctrl: ctrl}
	mock.recorder = &MockTodoServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockTodoService) EXPECT() *MockTodoServiceMockRecorder {
	return m.recorder
}

// GetAllTasksService mocks base method.
func (m *MockTodoService) GetAllTasksService() ([]models.Todo, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetAllTasksService")
	ret0, _ := ret[0].([]models.Todo)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetAllTasksService indicates an expected call of GetAllTasksService.
func (mr *MockTodoServiceMockRecorder) GetAllTasksService() *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetAllTasksService", reflect.TypeOf((*MockTodoService)(nil).GetAllTasksService))
}

// InsertService mocks base method.
func (m *MockTodoService) InsertService(arg0 models.Todo) (bool, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "InsertService", arg0)
	ret0, _ := ret[0].(bool)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// InsertService indicates an expected call of InsertService.
func (mr *MockTodoServiceMockRecorder) InsertService(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InsertService", reflect.TypeOf((*MockTodoService)(nil).InsertService), arg0)
}
