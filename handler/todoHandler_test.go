package handler

import (
	services "backend/mocks/service"
	"backend/models"
	"bytes"
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"net/http/httptest"
	"testing"
)

var mockService *services.MockTodoService
var handler TodoHandler

func setup(t *testing.T) func() {
	controller := gomock.NewController(t)

	mockService = services.NewMockTodoService(controller)

	handler = TodoHandler{mockService}

	return func() {
		defer controller.Finish()
	}
}

func TestTodoHandler_GetAllTasks(t *testing.T) {
	set := setup(t)
	defer set()

	router := fiber.New()
	router.Get("/todos", handler.GetAllTasks)

	var mockData = []models.Todo{
		{primitive.NewObjectID(), "Task 1"},
		{primitive.NewObjectID(), "Task 2"},
		{primitive.NewObjectID(), "Task 3"},
	}

	mockService.EXPECT().GetAllTasksService().Return(mockData, nil)

	req := httptest.NewRequest("GET", "/todos", nil)
	resp, _ := router.Test(req, 1)

	assert.Equal(t, 200, resp.StatusCode)
}
func TestTodoHandler_CreateTask(t *testing.T) {
	set := setup(t)
	defer set()

	router := fiber.New()
	router.Post("/todos", handler.CreateTask)

	todo := models.Todo{ID: primitive.NewObjectID(), Task: "Task 1"}
	todoJSON, _ := json.Marshal(todo)
	//fmt.Println(todo)
	//fmt.Println(todoJSON)
	//fmt.Println(bytes.NewBuffer(todoJSON))
	mockService.EXPECT().InsertService(todoJSON).Return(true, nil)
	req := httptest.NewRequest("POST", "/todos", bytes.NewBuffer(todoJSON))
	resp, _ := router.Test(req, 1)

	assert.Equal(t, 200, resp.StatusCode)

}
