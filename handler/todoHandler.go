package handler

import (
	"backend/models"
	"backend/services"
	"github.com/gofiber/fiber/v2"
	"net/http"
)

type TodoHandler struct {
	Service services.TodoService
}

func (h TodoHandler) CreateTask(c *fiber.Ctx) error {
	var todo models.Todo

	if err := c.BodyParser(&todo); err != nil {
		return c.Status(http.StatusBadRequest).JSON(err.Error())
	}

	result, err := h.Service.InsertService(todo)

	if err != nil || result == false {
		return err
	}

	return c.Status(http.StatusCreated).JSON(result)
}

func (h TodoHandler) GetAllTasks(c *fiber.Ctx) error {
	result, err := h.Service.GetAllTasksService()

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(err.Error())
	}

	return c.Status(http.StatusOK).JSON(result)
}
